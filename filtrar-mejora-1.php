<?php
include 'conexionDB.php';

include 'lib.php';

//Get ultima jornada
$ultimaJornada = 0;
$sql = "SELECT DISTINCT jornada FROM players_historico order by jornada desc LIMIT 1";
$resultado = $mysqli->query($sql);
while($row = $resultado->fetch_assoc()) {
    $ultimaJornada = $row['jornada'];
}

//Minimo partidos jugados
if($ultimaJornada > 0){
    $minimoPartidos =  ($ultimaJornada * 10) / 100;
}else{
    $minimoPartidos = 1;
}


$dinero = str_replace(".", "", $_POST['dinero']) ;
$criterio = $_POST['criterio'];

$valueTeam = str_replace(".", "", $_POST['valueTeam']) ;
$totalPresupuesto = $dinero - $valueTeam;

$pointsTeam = $_POST['pointsTeam'];
$mediaTeam = $_POST['mediaTeam'];
$rachaTeam = $_POST['rachaTeam'];

$formacion = $_POST['formacion'];

if($formacion > 0){
    $explodeFormacion = explode('-',$formacion);
    $alineacion['keeper'] = 1;
    $alineacion['defender'] = $explodeFormacion[0];
    $alineacion['midfielder'] = $explodeFormacion[1];
    $alineacion['striker'] = $explodeFormacion[2];
}

if($criterio == 'media'){
    $criteroSelect = '(points/partidos_jugados) criterio';
}elseif($criterio == 'racha'){
    $criteroSelect = 'racha as criterio';
}elseif($criterio == 'puntos'){
    $criteroSelect = 'points as criterio';
}


$arrayOldTeam = array();
$arrayNewTeam = array();
$arrayPlayers = array();
$sql = "SELECT idPlayer FROM team_compare";
$resultado = $mysqli->query($sql);
while($row = $resultado->fetch_assoc()) {
   $idP = $row['idPlayer'];
    $sql2 = "SELECT * FROM players where id=$idP";
    $resultado2 = $mysqli->query($sql2);

    //Guardamos datos de antiguo equipo
    while($row2 = $resultado2->fetch_assoc()) {
        $positionPlayer = $row2['position'];
        $row2['media'] = 0;
        if($row2['partidos_jugados'] > 0) {
            $row2['media'] = $row2['points'] / $row2['partidos_jugados'];
        }
        $row2['criterio'] = 0;
        if($criterio == 'media'){
            if($row2['partidos_jugados'] > 0) {
                $row2['criterio'] = $row2['points'] / $row2['partidos_jugados'];
            }
        }elseif($criterio == 'racha'){
            $row2['criterio'] = $row2['racha'];
        }elseif($criterio == 'puntos'){
            $row2['criterio'] = $row2['points'];
        }
        $arrayOldTeam[] = $row2;
    }

    //Guardamos los jugadores ordenados por criterio
    $sqlBest = "SELECT id, value, $criteroSelect, name, points,partidos_jugados,position,racha FROM players where position='$positionPlayer' and status='ACTIVE' and partidos_jugados>='$minimoPartidos' order by criterio desc";
    $resultadoBest = $mysqli->query($sqlBest);
    while($row2 = $resultadoBest->fetch_assoc()) {
        if($row2['partidos_jugados'] > 0){
            $row2['media'] = $row2['points'] / $row2['partidos_jugados'];
        }else{
            $row2['media'] = 0;
        }

        $arrayPlayers[$positionPlayer][] = $row2;
    }
}

//Comparamos el antiguo equipo con el array de jugadores ordenados por el criterio elegido
$arrayOldTeam = shuffle_assoc($arrayOldTeam);

foreach($arrayOldTeam as $key => $player){
        $valueP = $player['value'];
        $criterioP = $player['criterio'];
        $position = $player['position'];

        //Cogemos solo el array de la posicion que nos interesa
        $arrayByPos = $arrayPlayers[$position];
        $numByPosition = $alineacion[$position];
        $jugadorCambiado = 0;
        //Empezamos la comparacion con nuestros jugadores
        foreach($arrayByPos as $playerAll){
            $existePlayer = 0;
            //Comprobamos que el jugador que estamos viendo para añadir no lo tenemos ya añadido...
            if(isset($arrayNewTeam[$position])){
                $countNew = count($arrayNewTeam[$position]);
                foreach($arrayNewTeam[$position] as $player1Check){
                    if($player1Check['id'] == $playerAll['id'])
                        $existePlayer = 1;
                }
            }else{
                $countNew = 0;
            }

            if($existePlayer == 0 and $countNew < $numByPosition){
                if(($playerAll['criterio'] > $criterioP) and ((($totalPresupuesto + $valueP) - $playerAll['value']) >= 0)){
                    $arrayNewTeam[$position][] = $playerAll;
                    $totalPresupuesto = ($totalPresupuesto + $valueP) - $playerAll['value'];
                    $jugadorCambiado = 1;
                    break;
                }
            }

        }

        //Si no tenemos presupuesto rellenamos con lo antiguo
        $existePlayer2 = 0;
        if($jugadorCambiado == 0 and $existePlayer == 0){
            if(isset($arrayNewTeam[$position])){
                foreach($arrayNewTeam[$position] as $player1Check){
                    if($player1Check['id'] == $player['id'])
                        $existePlayer2 = 1;
                }
            }

            if($existePlayer2 == 0){
                $arrayNewTeam[$position][] = $player;
            }else{

                foreach($arrayPlayers[$player['position']] as $playerAll){
                    $existePlayer3 = 0;
                    foreach($arrayNewTeam[$position] as $player1Check){
                        if($player1Check['id'] == $playerAll['id'])
                            $existePlayer3 = 1;
                    }

                    if($existePlayer3 == 0){
                        if(((($totalPresupuesto + $player['value']) - $playerAll['value']) >= 0)){
                            $arrayNewTeam[$position][] = $playerAll;
                            $totalPresupuesto = ($totalPresupuesto + $player['value']) - $playerAll['value'];
                            break;
                        }
                    }
                }
            }
        }



}
$data = '';

$arrayNewTeam = orderByPosition($arrayNewTeam,0);
if(count($arrayNewTeam)>0){
    $mediaTotal = 0;
    $puntosTotal = 0;
    $valueTotal = 0;
    $rachaTotal = 0;

    $data .= "<h4>Solución 1</h4>";
    foreach ($arrayNewTeam as $equipazo){
        foreach ($equipazo as $player){
            $data .= $player['name'];
            $data .= "<br>";
            $mediaTotal = $mediaTotal + $player['media'];
            $puntosTotal = $puntosTotal + $player['points'];
            $valueTotal = $valueTotal + $player['value'];
            if($criterio == 'racha') {
                $rachaTotal = $rachaTotal + $player['racha'];
            }
        }
    }

    $data .= "Puntos: ".$puntosTotal;
    $data .= "<br>";
    $data .= "Media: ".number_format($mediaTotal, 3, ',', ' ');
    $data .= "<br>";
    if($criterio == 'racha') {
        $data .= "Racha: " . $rachaTotal;
        $data .= "<br>";
    }
    $data .= "Valor: ".number_format($valueTotal, 0, ',', '.').'€';
    $data .= "<br>";
    $data .= " <form>";
    if($criterio == 'racha') {
        $data .= "<input type='hidden' name='criterioValue' id='criterioValue' value='$rachaTotal'>";
    }
    if($criterio == 'media'){
        $data .= "<input type='hidden' name='criterioValue' id='criterioValue' value='$mediaTotal'>";

    }
    if($criterio == 'puntos'){
        $data .= "<input type='hidden' name='criterioValue' id='criterioValue' value='$puntosTotal'>";

    }
    $data .= " </form>";

    echo $data;
}
