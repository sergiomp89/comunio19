<?
function getErrors($error){
	switch ($error) {
		case '1':
			return 'La jornada ya existe';
			break;
		case '2':
			return 'Falló la consulta';
			break;
		case '3':
			return 'Falló el backup';
			break;
		case '4':
			return 'Falló al insertar backup';
			break;
		case '5':
			return 'Falló al insertar jornada puntos';
			break;
		case '6':
			return 'Falló al actualizar tabla jugadores';
			break;	
		case '7':
			return 'Fallo al limpiar la tabla de jugadores';
			break;	
		case '8':
			return 'Fallo al recuperar backup';
			break;	
		case '9':
			return 'Fallo al insertar backup en jugadores';
			break;	
		case '10':
			return 'No estas borrando la ultima jornada';
			break;		
		case '11':
			return 'Fallo a borrar jornada';
			break;
		case '12':
			return 'Fallo al vaciar tabla backup';
			break;			
		case '13':
			return 'Fallo al insertar once ideal';
			break;
		case '14':
			return 'La jornada no puede ser 0';
			break;
        case '15':
            return 'Falló al actualizar tabla jugadores histórico';
            break;
        case '16':
            return 'Fallo al borrar jornada jugadores histórico';
            break;
        default:
			# code...
			break;
	}
}

function getSuccess($success){
	switch ($success) {
		case '1':
			return 'Backup recuperado';
			break;
		case '2':
			return 'Puntos actualizados';
			break;	
		case '3':
			return 'Jornada borrada';
			break;
        case '4':
            return 'Equipos actualizados';
            break;

        default:
			# code...
			break;
	}
}

function getPosition($position){
	switch ($position) {
		case 'keeper':
			return 'Portero';
			break;
		case 'defender':
			return 'Defensa';
			break;	
		case 'midfielder':
			return 'Centrocampista';
			break;		
		case 'striker':
			return 'Delantero';
			break;	
		default:
			# code...
			break;
	}
}

function getPositionColor($position){
	switch ($position) {
		case 'keeper':
			return '<span class="pos-1"></span>';
			break;
		case 'defender':
			return '<span class="pos-2"></span>';
			break;	
		case 'midfielder':
			return '<span class="pos-3"></span>';
			break;		
		case 'striker':
			return '<span class="pos-4"></span>';
			break;	
		default:
			# code...
			break;
	}
}

function getStatus($status){
	switch ($status) {
		case 'ACTIVE':
			return '<img src="images/ok.png">';
			break;
		case 'YELLOW_BANNED':
			return '<img src="images/acum_tarjetas.png">';
			break;	
		case 'INJURED':
			return '<img src="images/lesion.png">';
			break;		
		case 'RETIRED':
			return 'Retirado';
			break;	
		case 'RED_BANNED':
			return '<img src="images/tarjeta_roja.png">';
			break;
		case 'WEAKENED':
			return '<img src="images/molestias.png">';
			break;	
		case 'SUSPENDED':
			return 'Suspendido';
			break;	
		default:
			# code...
			break;
	}
}

function getArrayFormacion(){
    $formacion = ['4-4-2','3-4-3','3-5-2','4-3-3','4-5-1'];
    return $formacion;
}


function shuffle_assoc($list) {
    if (!is_array($list)) return $list;

    $keys = array_keys($list);
    shuffle($keys);
    $random = array();
    foreach ($keys as $key) {
        $random[$key] = $list[$key];
    }
    return $random;
}

function orderByPosition($array2,$type){
    $array = array();
    $array['keeper'] = array();
    $array['defender'] = array();
    $array['midfielder'] = array();
    $array['striker'] = array();
    if($type == 0){
        foreach ($array2 as $player){
            foreach ($player as $p){
                $array[$p['position']][] = $p;

            }
        }
    }else{
        foreach ($array2 as $p){
                $array[$p['position']][] = $p;
        }
    }

    return $array;
}

?>