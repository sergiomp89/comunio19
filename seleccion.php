<?
include 'header.php';
?> 
<body>
 <div id="wrapper">
  <?
  include 'menu.php';
  ?> 
        

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Construir equipo</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <!-- /.panel -->

                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <form id='filtrar' method="POST">
                            <div class="row">

                                    <legend>Filtros</legend>
                                    <div class="col-sm-2">
                                        Jornada:<br />
                                        <select name="jornada" id="jornada" class="form-control" required>
                                            <option value="" >Elige jornada</option>

                                            <?
                                            $sql = "SELECT DISTINCT jornada FROM players_historico order by jornada asc ";
                                            $resultado = $mysqli->query($sql);
                                            while($row = $resultado->fetch_assoc()) {
                                                ?>
                                                <option value="<?=$row['jornada']?>"><?=$row['jornada']?></option>
                                                <?
                                            }
                                            ?>

                                        </select>

                                    </div>
                                        <div class="col-sm-2">
                                           <!--desplegable para el numero de jugadores(hacerlo obligatorio)-->
                                               Numero de jugadores:<br />
                                                <input type="number" name="numero" id="numero" class="form-control" required>
                                        </div>
                                        <div class="col-sm-2">
                                               Presupuesto:<br />
                                            <input type="text" name="dinero" id="dinero" class="dinero form-control" required>
                                        </div>
                                        <div class="col-sm-2">
                                            <!--desplegable para elegir la formación-->
                                            Tipo de formación:<br />
                                            <select name="formacion" id="formacion" class="form-control" required>
                                                <option value="" selected="selected">Elige formación</option>
                                                <option value="4-4-2" >4-4-2</option>
                                                <option value="3-4-3">3-4-3</option>
                                                <option value="3-5-2">3-5-2</option>
                                                <option value="4-3-3">4-3-3</option>
                                                <option value="4-5-1">4-5-1</option>
                                            </select>

                                        </div>
                                        <div class="col-sm-3">
                                               Criterio a maximizar:<br /> 
                                               <select name="criterio" id="criterio" class="form-control">
                                                   <option value="media" selected="selected">Media</option>
                                                   <option value="racha">Racha</option>
                                                   <option value="puntos">Numero de puntos</option>
                                                   <option value="costePunto">Coste por punto</option>
                                                   <option value="costePuntoMedio">Coste por punto medio</option>

                                               </select>

                                        </div>




                                        <div class="col-sm-12"> 
                                        <legend>Filtros optativos</legend>
                                        <!--Todas las opciones de este submenu deben ser optativas-->

                                                <div class="col-sm-3">
                                                   <!--desplegable para elegir la formación-->
                                                       Máximo jugadores por equipo:<br /> 
                                                       <select name="maxPorEquipo" id="maxPorEquipo" class="form-control">
                                                           <option value="0" selected="selected">Elige max jugadores</option>
                                                           <option value="1" >1</option>
                                                           <option value="2">2</option>
                                                           <option value="3">3</option>
                                                       </select>

                                                </div>
                                        </div>

                            </form>
                            </div>
                            <div class="row">
                                <legend>Constructivo</legend>
                                <div class="col-sm-2">
                                    Tiempo (segundos):<br />
                                    <form id='constructivo' method="POST">
                                    <input type="text" class="form-control veces" name="veces" id="veces" required>
                                    </form>
                                </div>
                                <div class="col-sm-2">
                                    <input type="button" id="submit1F" value="Constru 1F" class="btn btn-primary">
                                    <img class="loading1F" src="images/loading.gif" style="display: none" >

                                </div>
                                <div class="col-sm-2">
                                    <div id="divResultados1F" style="display: none">
                                        <a class="btn btn-success" role="button" onclick="showSolucion('resultados1F')">Ver solución 1F</a>
                                        <div id="resultados1F" style="display: none">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <input type="button" id="submitTmax" value="Constru Tmax" class="btn btn-primary">
                                    <img class="loadingTmax" src="images/loading.gif" style="display: none" >

                                </div>
                                <div class="col-sm-2">
                                    <div id="divResultadosTmax" style="display: none">
                                        <a class="btn btn-success" role="button" onclick="showSolucion('resultadosTmax')">Ver solución Tmax</a>
                                        <div id="resultadosTmax" style="display: none">
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <legend>Constructivo + Busqueda Local</legend>
                                <div class="col-sm-2">
                                    Tiempo (segundos):<br />
                                    <form id='local' method="POST">
                                        <input type="text" class="form-control veces" name="vecesCL" id="vecesCL" required>
                                    </form>
                                </div>

                                <div class="col-sm-2">
                                    <input type="button" id="submitConsLocal1" value="CL1" class="btn btn-primary">
                                    <img class="loadingLocal1" src="images/loading.gif" style="display: none" >

                                </div>
                                <div class="col-sm-2">
                                    <div id="divResultadosConsLocal1" style="display: none">
                                        <a class="btn btn-success" role="button" onclick="showSolucion('resultadosConsLocal1')">Ver solución CL 1</a>
                                        <div id="resultadosConsLocal1" style="display: none">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <input type="button" id="submitConsLocal2" value="CL2" class="btn btn-primary">
                                    <img class="loadingLocal2" src="images/loading.gif" style="display: none" >

                                </div>
                                <div class="col-sm-2">
                                    <div id="divResultadosConsLocal2" style="display: none">
                                        <a class="btn btn-success" role="button" onclick="showSolucion('resultadosConsLocal2')">Ver solución CL 2</a>
                                        <div id="resultadosConsLocal2" style="display: none">
                                        </div>
                                    </div>
                                </div>
                            </div>

                    <div class="row">
                        <legend>Multiarranque</legend>
                        <div class="col-sm-2">
                            Tiempo (segundos):<br />
                            <form id="multi" method="POST">
                                <input type="text" class="form-control veces" name="vecesMulti" id="vecesMulti" required>
                            </form>
                        </div>

                        <div class="col-sm-2">
                            <input type="button" id="submitMulti1" value="Multi1" class="btn btn-primary">
                            <div class="loadingDiv" style="margin:0 auto">
                                <img class="loadingMulti1" src="images/loading.gif" style="display: none" >
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div id="divResultadosMulti1" style="display: none">
                                <a class="btn btn-success" role="button" onclick="showSolucion('resultadosMulti1')">Ver Multi 1</a>
                                <div id="resultadosMulti1" style="display: none">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <input type="button" id="submitMulti2" value="Multi2" class="btn btn-primary">
                            <img class="loadingMulti2" src="images/loading.gif" style="display: none" >

                        </div>
                        <div class="col-sm-2">
                            <div id="divResultadosMulti2" style="display: none">
                                <a class="btn btn-success" role="button" onclick="showSolucion('resultadosMulti2')">Ver Multi 2</a>
                                <div id="resultadosMulti2" style="display: none">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <legend>VNS</legend>
                        <div class="col-sm-2">
                            <form id="vns" method="POST">
                            Tiempo (segundos):<br />
                                <input type="text" class="form-control veces" name="vecesVns" id="vecesVns" required><br />

                            Kmax:<br />
                                <input type="text" class="form-control veces" name="kmax" id="kmax" required><br />
                            </form>
                        </div>

                        <div class="col-sm-2">
                            <input type="button" id="submitVns1" value="VNS1" class="btn btn-primary">
                            <div class="loadingDiv" style="margin:0 auto">
                                <img class="loadingVns1" src="images/loading.gif" style="display: none" >
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div id="divResultadosVns1" style="display: none">
                                <a class="btn btn-success" role="button" onclick="showSolucion('resultadosVns1')">Ver VNS 1</a>
                                <div id="resultadosVns1" style="display: none">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <input type="button" id="submitVns2" value="VNS2" class="btn btn-primary">
                            <img class="loadingVns2" src="images/loading.gif" style="display: none" >

                        </div>
                        <div class="col-sm-2">
                            <div id="divResultadosVns2" style="display: none">
                                <a class="btn btn-success" role="button" onclick="showSolucion('resultadosVns2')">Ver VNS 2</a>
                                <div id="resultadosVns2" style="display: none">
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

            </div>

        </div>
 </div>

</body>
  <?
include 'footer.php';

?>

<script>
    function showSolucion(id) {
        if (document.getElementById) {
            var el = document.getElementById(id);
            el.style.display = (el.style.display == 'none') ? 'block' : 'none';
            window.scrollTo(0,document.body.scrollHeight);

        }
    }

    $(document).ready(function(){
        $('.dinero').mask('000.000.000.000.000', {reverse: true});
        $('.veces').mask('000.000.000.000.000', {reverse: true});
    });

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $( "#submit1F" ).click(function(e) {
            if (($("#filtrar").valid()) && ($("#constructivo").valid()))  {
                $('.loading1F').css({display: "block"});

                var veces = $("#veces").val();


                $('#resultados').html('');
                $('.loading').css({display: "block"});

                var numero = $("#numero").val();
                var dinero = $("#dinero").val();
                var criterio = $("#criterio").val();

                var maxPorEquipo = $("#maxPorEquipo").val();
                var formacion = $("#formacion").val();
                var jornada = $("#jornada").val();


                var parametros = {
                    "numero": numero,
                    "dinero": dinero,
                    "criterio": criterio,
                    "veces": veces,
                    "maxPorEquipo": maxPorEquipo,
                    "formacion": formacion,
                    "jornada": jornada
                };
                $.ajax({
                    url: "filtrar-1F.php",
                    data: parametros,
                    dataType: "html",
                    type: "post",
                    success: function (data) {
                        $('.loading1F').css({display: "none"});
                        $('#resultados1F').html(data);
                        $('#divResultados1F').show();

                    }
                });


            }
            //e.preventDefault();




        });

        $( "#submitTmax" ).click(function(e) {
            if (($("#filtrar").valid()) && ($("#constructivo").valid())) {
                $('.loadingTmax').css({display: "block"});

                var numero = $("#numero").val();
                var dinero = $("#dinero").val();
                var criterio = $("#criterio").val();
                var veces = $("#veces").val();

                var maxPorEquipo = $("#maxPorEquipo").val();
                var formacion = $("#formacion").val();

                var jornada = $("#jornada").val();


                var parametros = {
                    "numero": numero,
                    "dinero": dinero,
                    "criterio": criterio,
                    "veces": veces,
                    "maxPorEquipo": maxPorEquipo,
                    "formacion": formacion,
                    "jornada": jornada
                };
                $.ajax({
                    url: "filtrar-tmax.php",
                    data: parametros,
                    dataType: "html",
                    type: "post",
                    success: function (data) {
                        $('.loadingTmax').css({display: "none"});
                        $('#resultadosTmax').html(data);
                        $('#divResultadosTmax').show();

                    }
                });
            }
        });

        $( "#submitConsLocal1" ).click(function(e) {
            if (($("#filtrar").valid()) && ($("#local").valid())) {
                $('.loadingLocal1').css({display: "block"});

                var numero = $("#numero").val();
                var dinero = $("#dinero").val();
                var criterio = $("#criterio").val();
                var veces = $("#vecesCL").val();

                var maxPorEquipo = $("#maxPorEquipo").val();
                var formacion = $("#formacion").val();

                var jornada = $("#jornada").val();


                var parametros = {
                    "numero": numero,
                    "dinero": dinero,
                    "criterio": criterio,
                    "veces": veces,
                    "maxPorEquipo": maxPorEquipo,
                    "formacion": formacion,
                    "jornada": jornada
                };
                $.ajax({
                    url: "filtrar-cl1.php",
                    data: parametros,
                    dataType: "html",
                    type: "post",
                    success: function (data) {
                        $('.loadingLocal1').css({display: "none"});
                        $('#resultadosConsLocal1').html(data);
                        $('#divResultadosConsLocal1').show();

                    }
                });
            }
        });
        $( "#submitConsLocal2" ).click(function(e) {
            if (($("#filtrar").valid()) && ($("#local").valid())) {
                $('.loadingLocal2').css({display: "block"});

                var numero = $("#numero").val();
                var dinero = $("#dinero").val();
                var criterio = $("#criterio").val();
                var veces = $("#vecesCL").val();

                var maxPorEquipo = $("#maxPorEquipo").val();
                var formacion = $("#formacion").val();

                var jornada = $("#jornada").val();


                var parametros = {
                    "numero": numero,
                    "dinero": dinero,
                    "criterio": criterio,
                    "veces": veces,
                    "maxPorEquipo": maxPorEquipo,
                    "formacion": formacion,
                    "jornada": jornada
                };
                $.ajax({
                    url: "filtrar-cl2.php",
                    data: parametros,
                    dataType: "html",
                    type: "post",
                    success: function (data) {
                        $('.loadingLocal2').css({display: "none"});
                        $('#resultadosConsLocal2').html(data);
                        $('#divResultadosConsLocal2').show();

                    }
                });
            }
        });



        $( "#submitMulti1" ).click(function(e) {
            if (($("#filtrar").valid()) && ($("#multi").valid())) {
                $('.loadingMulti1').css({display: "block"});

                var numero = $("#numero").val();
                var dinero = $("#dinero").val();
                var criterio = $("#criterio").val();
                var veces = $("#vecesMulti").val();

                var maxPorEquipo = $("#maxPorEquipo").val();
                var formacion = $("#formacion").val();

                var jornada = $("#jornada").val();


                var parametros = {
                    "numero": numero,
                    "dinero": dinero,
                    "criterio": criterio,
                    "veces": veces,
                    "maxPorEquipo": maxPorEquipo,
                    "formacion": formacion,
                    "jornada": jornada
                };
                $.ajax({
                    url: "filtrar-multi1.php",
                    data: parametros,
                    dataType: "html",
                    type: "post",
                    success: function (data) {
                        $('.loadingMulti1').css({display: "none"});
                        $('#resultadosMulti1').html(data);
                        $('#divResultadosMulti1').show();

                    }
                })
            }
        });


        $( "#submitMulti2" ).click(function(e) {
            if (($("#filtrar").valid()) && ($("#multi").valid())) {
                $('.loadingMulti2').css({display: "block"});

                var numero = $("#numero").val();
                var dinero = $("#dinero").val();
                var criterio = $("#criterio").val();
                var veces = $("#vecesMulti").val();

                var maxPorEquipo = $("#maxPorEquipo").val();
                var formacion = $("#formacion").val();

                var jornada = $("#jornada").val();


                var parametros = {
                    "numero": numero,
                    "dinero": dinero,
                    "criterio": criterio,
                    "veces": veces,
                    "maxPorEquipo": maxPorEquipo,
                    "formacion": formacion,
                    "jornada": jornada
                };
                $.ajax({
                    url: "filtrar-multi2.php",
                    data: parametros,
                    dataType: "html",
                    type: "post",
                    success: function (data) {
                        $('.loadingMulti2').css({display: "none"});
                        $('#resultadosMulti2').html(data);
                        $('#divResultadosMulti2').show();

                    }
                });
            }
        });



        $( "#submitVns1" ).click(function(e) {
            if (($("#filtrar").valid()) && ($("#vns").valid())) {
                $('.loadingVns1').css({display: "block"});

                var numero = $("#numero").val();
                var dinero = $("#dinero").val();
                var criterio = $("#criterio").val();
                var veces = $("#vecesVns").val();

                var maxPorEquipo = $("#maxPorEquipo").val();
                var formacion = $("#formacion").val();
                var kmax = $("#kmax").val();

                var jornada = $("#jornada").val();


                var parametros = {
                    "numero": numero,
                    "dinero": dinero,
                    "criterio": criterio,
                    "veces": veces,
                    "maxPorEquipo": maxPorEquipo,
                    "formacion": formacion,
                    "kmax": kmax,
                    "jornada": jornada
                };
                $.ajax({
                    url: "filtrar-vns1.php",
                    data: parametros,
                    dataType: "html",
                    type: "post",
                    success: function (data) {
                        $('.loadingVns1').css({display: "none"});
                        $('#resultadosVns1').html(data);
                        $('#divResultadosVns1').show();

                    }
                })
            }
        });


        $( "#submitVns2" ).click(function(e) {
            if (($("#filtrar").valid()) && ($("#vns").valid())) {
                $('.loadingVns2').css({display: "block"});

                var numero = $("#numero").val();
                var dinero = $("#dinero").val();
                var criterio = $("#criterio").val();
                var veces = $("#vecesVns").val();

                var maxPorEquipo = $("#maxPorEquipo").val();
                var formacion = $("#formacion").val();
                var kmax = $("#kmax").val();

                var jornada = $("#jornada").val();


                var parametros = {
                    "numero": numero,
                    "dinero": dinero,
                    "criterio": criterio,
                    "veces": veces,
                    "maxPorEquipo": maxPorEquipo,
                    "formacion": formacion,
                    "kmax": kmax,
                    "jornada": jornada
                };
                $.ajax({
                    url: "filtrar-vns2.php",
                    data: parametros,
                    dataType: "html",
                    type: "post",
                    success: function (data) {
                        $('.loadingVns2').css({display: "none"});
                        $('#resultadosVns2').html(data);
                        $('#divResultadosVns2').show();

                    }
                });
            }
        });



    });
</script>

<script type="text/javascript">

    function mejora1(){

        if($("#formacion").val() != 0){
            var dinero = $("#dinero").val();
            var criterio = $("#criterio").val();
            var formacion = $("#formacion").val();



            var p = $("#p").val();
            var v = $("#v").val();
            var r = $("#r").val();
            var m = $("#m").val();

            var parametros = {
                "dinero" : dinero,
                "criterio" : criterio,
                "mediaTeam" : m,
                "valueTeam": v,
                "rachaTeam": r,
                "pointsTeam": p,
                "formacion": formacion
            };
            $.ajax({
                url: "filtrar-mejora-1.php",
                data: parametros,
                dataType:"html",
                type: "post",
                success: function(data){
                    $('.loading').css({ display: "none" });
                    $('#resultadosMejora1').html(data);
                    $('#divMejora3').css({display: "block"});

                }
            });


        }else{
            alert("Debe elegir una formación");
        }


    }

    function mejora2(){
        var dinero = $("#dinero").val();
        var criterio = $("#criterio").val();


        var p = $("#p").val();
        var v = $("#v").val();
        var r = $("#r").val();
        var m = $("#m").val();

        var parametros = {
            "dinero" : dinero,
            "criterio" : criterio,
            "mediaTeam" : m,
            "valueTeam": v,
            "rachaTeam": r,
            "pointsTeam": p

        };
        $.ajax({
            url: "filtrar-mejora-2.php",
            data: parametros,
            dataType:"html",
            type: "post",
            success: function(data){

                $('.loading').css({ display: "none" });
                $('#resultadosMejora2').html(data);
                $('#divMejora4').css({display: "block"});

            }
        });


    }


    function mejora3(){

        $('#resultadosMejora3').html('');
        $('.loadingMejora3').css({display: "block"});

            var dinero = $("#dinero").val();
            var criterio = $("#criterio").val();
            var formacion = $("#formacion").val();
            var criterioValue = $("#criterioValue").val();

        var numero = $("#numero").val();


            var p = $("#p").val();
            var v = $("#v").val();
            var r = $("#r").val();
            var m = $("#m").val();


            var parametros = {
                "dinero" : dinero,
                "criterio" : criterio,
                "criterioValue" : criterioValue,
                "mediaTeam" : m,
                "valueTeam": v,
                "rachaTeam": r,
                "pointsTeam": p,
                "formacion": formacion,
                "numero": numero
            };
            $.ajax({
                url: "filtrar-bvns-1.php",
                data: parametros,
                dataType:"html",
                type: "post",
                success: function(data){
                    $('.loadingMejora3').css({ display: "none" });
                    $('#resultadosMejora3').html(data);
                }
            });



    }

    function mejora4(){
        var dinero = $("#dinero").val();
        var criterio = $("#criterio").val();
        var criterioValue = $("#criterioValue2").val();
        var numero = $("#numero").val();

        var p = $("#p").val();
        var v = $("#v").val();
        var r = $("#r").val();
        var m = $("#m").val();

        var parametros = {
            "dinero" : dinero,
            "criterio" : criterio,
            "mediaTeam" : m,
            "valueTeam": v,
            "rachaTeam": r,
            "pointsTeam": p,
            "criterioValue" : criterioValue,
            "numero": numero
        };
        $.ajax({
            url: "filtrar-bvns-2.php",
            data: parametros,
            dataType:"html",
            type: "post",
            success: function(data){

                $('.loading').css({ display: "none" });
                $('#resultadosMejora4').html(data);
            }
        });
    }

</script>

<script>
$(document).ready(function(){
    $('#tableEquipos').DataTable({
    	paging: false,
    	"order": [[ 1, "asc" ]],
    	"language": {
                "url": "dist/spanish.lang"
            }
    });
});
</script>
