<?
	include 'header.php';
?> 
<body>
 <div id="wrapper">
<?
  	include 'menu.php';
?>

<div id="page-wrapper">
<br><br>
<?


if(isset($_GET['error'])){?>
<div class="alert alert-danger">
	  <strong><?=getErrors($_GET['error']);?></strong>.
	</div>
<?}

if(isset($_GET['success'])){?>
<div class="alert alert-success">
	  <strong><?=getSuccess($_GET['success']);?></strong>.
	</div>
<?}?>
	<br><br>
	<form action="calcularPuntos.php" method="post">
		<fieldset>
		<legend>Actualizar base de datos</legend>
            <span title="Introduce datos en la base de datos" data-toggle="tooltip" data-placement="top">
		    <button type="submit" class="btn btn-success" data-toggle="confirmation" data-title="¿Estás seguro?" data-btn-ok-label="Sí" data-btn-cancel-label="No" >Actualizar</button>
		    <span>
        </fieldset>
	</form>

    <?php
    $sql = "SELECT DISTINCT jornada FROM players_historico order by jornada asc";
    $resultado = $mysqli->query($sql);
        if ($resultado->num_rows > 0) {?>

                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Jornadas</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <!-- /.panel -->

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped" id="tableEquipos">
                                            <thead>
                                            <tr>
                                                <th>Jornada</th>
                                                <th>Estado</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?
                                            while($row = $resultado->fetch_assoc()) {
                                                ?>
                                                <tr>

                                                    <td><?=$row['jornada']?></td>
                                                    <td><img src="images/ok.png"></td>
                                                </tr>
                                                <?
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.col-lg-4 (nested) -->
                                <div class="col-lg-8">
                                    <div id="morris-bar-chart"></div>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->


                </div>

        <?}

    ?>

	<br><br>

	<form action="eliminarJornada.php" method="post">
		<fieldset>
		<legend>Eliminar jornada</legend>
		  <div class="form-group">
		    <label for="jornada">Jornada:</label>
		    <input type="text" class="form-control" style="width: 65px;" name="jornada" required>
		  </div>
            <span title="The tooltip" data-toggle="tooltip" data-placement="top">

		        <button type="submit" class="btn btn-danger"  data-toggle="confirmation" data-title="¿Estás seguro?" data-btn-ok-label="Sí" data-btn-cancel-label="No">Eliminar</button>
            </span>
		 </fieldset>
	</form>




	<br><br>



</div>

  </body>
  <?
include 'footer.php';?>
<script>
$('[data-toggle=confirmation]').confirmation({
  rootSelector: '[data-toggle=confirmation]',
  // other options
});

</script>
