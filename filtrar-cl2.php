<?php
include 'conexionDB.php';
include 'lib.php';
$dinero = str_replace(".", "", $_POST['dinero']) ;
$numero = $_POST['numero'];
$criterio = $_POST['criterio'];
if(isset($_POST['veces']))
    $veces =str_replace(".", "", $_POST['veces']) ;

$maxPorEquipo = $_POST['maxPorEquipo'];
$formacion = $_POST['formacion'];
$jornada = $_POST['jornada'];

if($formacion > 0){
    $explodeFormacion = explode('-',$formacion);
    $alineacion['keeper'] = 1;
    $alineacion['defender'] = $explodeFormacion[0];
    $alineacion['midfielder'] = $explodeFormacion[1];
    $alineacion['striker'] = $explodeFormacion[2];
}


//Get ultima jornada
$ultimaJornada = 0;
$sql = "SELECT DISTINCT jornada FROM players_historico order by jornada desc LIMIT 1";
$resultado = $mysqli->query($sql);
while($row = $resultado->fetch_assoc()) {
    $ultimaJornada = $row['jornada'];
}

$data = '';
$arrayResultados = array();
$counter = 0;

$arrayJugadores = array();

//SELECCIONAR ALEATORIO DE BBDD
$sql = "SELECT ph.id, p.name, ph.value, ph.points, ph.partidos_jugados, ph.idTeam,ph.position,ph.status,ph.racha FROM players_historico ph, players p where ph.jornada=$jornada and ph.id=p.id"; //seleccionamos 11 aleatorios y comprobamos
$result = $mysqli->query($sql);
$totalMedia = 0;
$totalValor = 0;
$totalPuntos = 0;
while ($row = $result->fetch_assoc()) {
    $arrayJugadores[] = $row;
}

$arrayEquipos = array();
$counter = 0;
define('START_TIME', time());
//for ($i = 1; $i <= $veces; $i++) {
while (time() - START_TIME < $veces){
    shuffle($arrayJugadores);

    $arrayIdsJugadores = array();
    if($formacion > 0){
        //Keeper
        foreach ($arrayJugadores as $jugador){
            if($jugador['position'] == 'keeper' && $jugador['status'] == 'ACTIVE'){
                $arrayIdsJugadores[] = $jugador['id'];
                $jugador['media'] = 0;
                if($jugador['partidos_jugados'] > 0) {
                    $jugador['media'] = $jugador['points'] / $jugador['partidos_jugados'];
                }
                $jugador['criterio'] = 0;
                if($criterio == 'media'){
                    if($jugador['partidos_jugados'] > 0) {
                        $jugador['criterio'] = $jugador['points'] / $jugador['partidos_jugados'];
                    }
                }elseif($criterio == 'racha'){
                    $jugador['criterio'] = $jugador['racha'];
                }elseif($criterio == 'puntos'){
                    $jugador['criterio'] = $jugador['points'];
                }
                $arrayEquipos[$counter][] = $jugador;
                break;
            }

        }
        //Defensas
        for ($d = 1; $d <= $alineacion['defender']; $d++) {
            foreach ($arrayJugadores as $jugador){
                if($jugador['position'] == 'defender' && !in_array($jugador['id'],$arrayIdsJugadores) && $jugador['status'] == 'ACTIVE'){
                    $arrayIdsJugadores[] = $jugador['id'];
                    $jugador['media'] = 0;
                    if($jugador['partidos_jugados'] > 0) {
                        $jugador['media'] = $jugador['points'] / $jugador['partidos_jugados'];
                    }
                    $jugador['criterio'] = 0;
                    if($criterio == 'media'){
                        if($jugador['partidos_jugados'] > 0) {
                            $jugador['criterio'] = $jugador['points'] / $jugador['partidos_jugados'];
                        }
                    }elseif($criterio == 'racha'){
                        $jugador['criterio'] = $jugador['racha'];
                    }elseif($criterio == 'puntos'){
                        $jugador['criterio'] = $jugador['points'];
                    }
                    $arrayEquipos[$counter][] = $jugador;
                    break;
                }

            }
        }
        //Medios
        for ($d = 1; $d <= $alineacion['midfielder']; $d++) {
            foreach ($arrayJugadores as $jugador){
                if($jugador['position'] == 'midfielder' && !in_array($jugador['id'],$arrayIdsJugadores) && $jugador['status'] == 'ACTIVE'){
                    $arrayIdsJugadores[] = $jugador['id'];
                    $jugador['media'] = 0;
                    if($jugador['partidos_jugados'] > 0) {
                        $jugador['media'] = $jugador['points'] / $jugador['partidos_jugados'];
                    }
                    $jugador['criterio'] = 0;
                    if($criterio == 'media'){
                        if($jugador['partidos_jugados'] > 0) {
                            $jugador['criterio'] = $jugador['points'] / $jugador['partidos_jugados'];
                        }
                    }elseif($criterio == 'racha'){
                        $jugador['criterio'] = $jugador['racha'];
                    }elseif($criterio == 'puntos'){
                        $jugador['criterio'] = $jugador['points'];
                    }
                    $arrayEquipos[$counter][] = $jugador;
                    break;
                }

            }
        }
        //Delanteros
        for ($d = 1; $d <= $alineacion['striker']; $d++) {
            foreach ($arrayJugadores as $jugador){
                if($jugador['position'] == 'striker' && !in_array($jugador['id'],$arrayIdsJugadores) && $jugador['status'] == 'ACTIVE'){
                    $arrayIdsJugadores[] = $jugador['id'];
                    $jugador['media'] = 0;
                    if($jugador['partidos_jugados'] > 0) {
                        $jugador['media'] = $jugador['points'] / $jugador['partidos_jugados'];
                    }
                    $jugador['criterio'] = 0;
                    if($criterio == 'media'){
                        if($jugador['partidos_jugados'] > 0) {
                            $jugador['criterio'] = $jugador['points'] / $jugador['partidos_jugados'];
                        }
                    }elseif($criterio == 'racha'){
                        $jugador['criterio'] = $jugador['racha'];
                    }elseif($criterio == 'puntos'){
                        $jugador['criterio'] = $jugador['points'];
                    }
                    $arrayEquipos[$counter][] = $jugador;
                    break;
                }

            }
        }

        if(count($arrayEquipos[$counter]) < $numero){
            $vecesRestantes = $numero - count($arrayEquipos[$counter]);
            for ($d = 1; $d <= $vecesRestantes; $d++) {
                $jugador = array_rand($arrayJugadores);
                $jugador = $arrayJugadores[$jugador];
                if((!in_array($jugador['id'],$arrayIdsJugadores)) && ($jugador['status'] == 'ACTIVE')){
                    $arrayEquipos[$counter][] = $jugador;
                }else{
                    $d--;
                }

            }
        }

    }else{
        for ($d = 1; $d <= $numero; $d++) {
            $jugador = array_rand($arrayJugadores);
            $jugador = $arrayJugadores[$jugador];
            if((!in_array($jugador['id'],$arrayIdsJugadores)) && ($jugador['status'] == 'ACTIVE')){
                $arrayEquipos[$counter][] = $jugador;
            }else{
                $d--;
            }

        }

    }
    $counter++;

    //}

    $arrayBestTeam = array();
    $counterTeam = 0;
    foreach ($arrayEquipos as  $equipo){
        $team = true;
        $equiposJugadores = array();
        $valueTeam = 0;
        $pointsTeam = 0;
        $totalMedia = 0;
        $racha = 0;
        foreach ($equipo as $jugador){
            $valueTeam = $valueTeam + $jugador['value'];
            $pointsTeam = $pointsTeam + $jugador['points'];
            if ($jugador['partidos_jugados'] > 0) {             //calculamos la media de cada jugador
                $media = $jugador['points'] / $jugador['partidos_jugados'];
            } else {
                $media = 0;
            }
            $totalMedia = $totalMedia + $media;

            $equiposJugadores[] = $jugador['idTeam'];
            $racha = $racha + $jugador['racha'];
        }




        //Check si hay mas jugadores del mismo equipo
        if($maxPorEquipo > 0){
            $team_array = array_count_values($equiposJugadores);
            while (list ($key, $val) = each ($team_array))
            {
                if($val >= $maxPorEquipo)
                    $team = false;
            }
        }

        if(($team) and ($valueTeam <= $dinero)){
            $arrayBestTeam[$counterTeam]['racha'] = $racha;
            $arrayBestTeam[$counterTeam]['equipo'] = $equipo;
            $arrayBestTeam[$counterTeam]['valueTeam'] = number_format($valueTeam, 0, ',', '.');
            $arrayBestTeam[$counterTeam]['pointsTeam'] = $pointsTeam;
            $arrayBestTeam[$counterTeam]['mediaTeam'] = number_format($totalMedia, 3, ',', ' ');
            $counterTeam++;
        }

    }



    if($criterio == 'media'){     // si seleccionamos como criterio la media, recorremos el array de soluciones buscando la que tenga mejor media
        $mediaResult = 0;
        $best = array();
        foreach ($arrayBestTeam as $result){
            if($result['mediaTeam'] > $mediaResult){
                unset($best);
                $mediaResult = $result['mediaTeam'];
                $best[] = $result;
            }
        }
    }

    if($criterio == 'racha'){
        $rachaResult = 0;
        $best = array();
        foreach ($arrayBestTeam as $result){
            if($result['racha'] > $rachaResult){
                unset($best);
                $rachaResult = $result['racha'];
                $best[] = $result;
            }
        }
    }

    if($criterio == 'puntos'){    // si seleccionamos como criterio los puntos, recorremos el array de soluciones buscando la fila q tenga el mejor puntos
        $puntosResult = 0;
        $best = array();
        foreach ($arrayBestTeam as $result){
            if($result['pointsTeam'] > $puntosResult){
                unset($best);
                $puntosResult = $result['pointsTeam'];
                $best[] = $result;
            }
        }
    }

    if($criterio == 'costePunto'){
        /*$puntosResult = 99999999999999999;
        $best = array();
        foreach ($arrayBestTeam as $result){
            $val = str_replace(".", "", $result['valueTeam']);
            $poi = str_replace(".", "", $result['pointsTeam']);

            $costePorPunto = $val / $poi;
            if($costePorPunto < $puntosResult){
                unset($best);
                $puntosResult = $costePorPunto;
                $best[] = $result;
            }
        }*/
    }

    if($criterio == 'costePuntoMedio'){
        /*$puntosResult = 99999999999999999;
        $best = array();
        foreach ($arrayBestTeam as $result){
            $costePorPuntoMedio = $result['mediaTeam'] / $result['valueTeam'];
            if($costePorPuntoMedio < $puntosResult){
                unset($best);
                $puntosResult = $costePorPuntoMedio;
                $best[] = $result;
            }
        }*/
    }

    if(count($best)>0){
        break;
    }
}

if(count($best)>0){

    $totalPresupuesto = $dinero - str_replace(".", "", $best[0]['valueTeam']);
    //Minimo partidos jugados
    if($ultimaJornada > 0){
        $minimoPartidos =  ($ultimaJornada * 10) / 100;
    }else{
        $minimoPartidos = 1;
    }

    if($criterio == 'media'){
        $criteroSelect = '(ph.points/ph.partidos_jugados) criterio';
    }elseif($criterio == 'racha'){
        $criteroSelect = 'ph.racha as criterio';
    }elseif($criterio == 'puntos'){
        $criteroSelect = 'ph.points as criterio';
    }

    //Guardamos los jugadores ordenados por criterio
    $sqlBest = "SELECT ph.id, ph.value, $criteroSelect, p.name, ph.points,ph.partidos_jugados,ph.position,ph.racha FROM players_historico ph, players p where ph.id=p.id and  ph.status='ACTIVE' and ph.partidos_jugados>='$minimoPartidos' and ph.jornada = $jornada order by ph.position,criterio desc";
    $resultadoBest = $mysqli->query($sqlBest);
    while($row2 = $resultadoBest->fetch_assoc()) {
        $positionPlayer = $row2['position'];
        if($row2['partidos_jugados'] > 0){
            $row2['media'] = $row2['points'] / $row2['partidos_jugados'];
        }else{
            $row2['media'] = 0;
        }

        $arrayPlayers[$positionPlayer][] = $row2;
    }

    foreach($arrayPlayers as $key => $player){
        $p = shuffle_assoc($player);
        $arrayPlayers[$key] = $p;
    }

    $arrayOldEquipoShuffle = shuffle_assoc($best[0]['equipo']);
    $arrayOldTeam = $best[0];
    $arrayOldTeam['equipo'] = $arrayOldEquipoShuffle;

    do {
        $hayCambios = count($arrayOldTeam['equipo']);

            foreach ($arrayOldTeam['equipo'] as $key => $player) {
                $valueP = $player['value'];

                $criterioP = $player['criterio'];
                $position = $player['position'];

                //Cogemos solo el array de la posicion que nos interesa
                $arrayByPos = $arrayPlayers[$position];

                //Empezamos la comparacion con nuestros jugadores
                foreach ($arrayByPos as $playerAll) {
                    $existePlayer = 0;
                    $existePlayer2 = 0;
                    if ($player['id'] != $playerAll['id']) {
                        foreach ($arrayOldTeam['equipo'] as $player1Check) {
                            if ($player1Check['id'] == $playerAll['id'])
                                $existePlayer2 = 1;
                        }

                        if (($playerAll['criterio'] > $criterioP) and ((($totalPresupuesto + $valueP) - $playerAll['value']) >= 0) and $existePlayer2 == 0) {
                            $arrayOldTeam['equipo'][$key] = $playerAll;
                            $totalPresupuesto = ($totalPresupuesto + $valueP) - $playerAll['value'];
                            $existePlayer = 1;
                            break;
                        }
                    }
                }
                if ($existePlayer == 0)
                    $hayCambios--;

            }

    }while($hayCambios > 0);


    $data = '';
    $arrayOldTeam2 = orderByPosition($best[0]['equipo'],1);
    $data .= "<h4>Solución Const</h4>";
    $mediaTotal = 0;
    $puntosTotal = 0;
    $valueTotal = 0;
    $rachaTotal = 0;

    foreach ($arrayOldTeam2 as $equipazo){
        foreach ($equipazo as $player){
            $data .= $player['name'];
            $data .= "<br>";
            $mediaTotal = $mediaTotal + $player['media'];
            $puntosTotal = $puntosTotal + $player['points'];
            $valueTotal = $valueTotal + $player['value'];
            if($criterio == 'racha') {
                $rachaTotal = $rachaTotal + $player['racha'];
            }
        }
    }

    $data .= "Puntos: ".$puntosTotal;
    $data .= "<br>";
    $data .= "Media: ".number_format($mediaTotal, 3, ',', ' ');
    $data .= "<br>";
    if($criterio == 'racha') {
        $data .= "Racha: " . $rachaTotal;
        $data .= "<br>";
    }
    $data .= "Valor: ".number_format($valueTotal, 0, ',', '.').'€';
    $data .= "<br>";
    $data .= " <form>";
    if($criterio == 'racha') {
        $data .= "<input type='hidden' name='criterioValue' id='criterioValue' value='$rachaTotal'>";
    }
    if($criterio == 'media'){
        $data .= "<input type='hidden' name='criterioValue' id='criterioValue' value='$mediaTotal'>";

    }
    if($criterio == 'puntos'){
        $data .= "<input type='hidden' name='criterioValue' id='criterioValue' value='$puntosTotal'>";

    }
    $data .= " </form>";

    $arrayOldTeam = orderByPosition($arrayOldTeam['equipo'],1);

   if(count($arrayOldTeam)>0){
        $mediaTotal = 0;
        $puntosTotal = 0;
        $valueTotal = 0;
        $rachaTotal = 0;

        $data .= "<h4>Solución 2</h4>";
        foreach ($arrayOldTeam as $equipazo){
            foreach ($equipazo as $player){
                $data .= $player['name'];
                $data .= "<br>";
                $mediaTotal = $mediaTotal + $player['media'];
                $puntosTotal = $puntosTotal + $player['points'];
                $valueTotal = $valueTotal + $player['value'];
                if($criterio == 'racha') {
                    $rachaTotal = $rachaTotal + $player['racha'];
                }
            }
        }

        $data .= "Puntos: ".$puntosTotal;
        $data .= "<br>";
        $data .= "Media: ".number_format($mediaTotal, 3, ',', ' ');
        $data .= "<br>";

        if($criterio == 'racha') {
            $data .= "Racha: " . $rachaTotal;
            $data .= "<br>";
        }
        $data .= "Valor: ".number_format($valueTotal, 0, ',', '.').'€';
        $data .= "<br>";

        if($criterio == 'racha') {
            $data .= "<input type='hidden' name='criterioValue2' id='criterioValue2' value='$rachaTotal'>";
        }
        if($criterio == 'media'){
            $data .= "<input type='hidden' name='criterioValue2' id='criterioValue2' value='$mediaTotal'>";

        }
        if($criterio == 'puntos'){
            $data .= "<input type='hidden' name='criterioValue2' id='criterioValue2' value='$puntosTotal'>";

        }

    }


}else{
    $data .= "<div class='row'>";
    $data .= "<div class='col-sm-12'>";
    $data .= "No hemos podido encontrar un equipo";
    $data .="</div>";
    $data .="</div>";
}


echo $data;

