<?
include 'header.php';
$idTeam = $_GET['id'];
?> 
<body>
 <div id="wrapper">
  <?
  include 'menu.php';
  ?> 
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <?
                        $sql = "SELECT e.name as nombreEquipo, SUM(j.points) as points, SUM(j.value) as value FROM teams e, players j WHERE j.idTeam=$idTeam and j.idTeam=e.id  GROUP BY j.idTeam";
                        $resultado = $mysqli->query($sql);          
                        $team= $resultado->fetch_assoc();

                        $sqlJugadores = "SELECT j.position, j.value, j.points, j.partidos_jugados,j.name FROM players j where idTeam=$idTeam order by position, points asc";
                        $resultadoJugadores = $mysqli->query($sqlJugadores);
                        while($rowJugadores = $resultadoJugadores->fetch_assoc()) {
                                if($rowJugadores['position'] == 'keeper'){
                                    $arrayJugadores[0][] = $rowJugadores;
                                }else if($rowJugadores['position'] == 'defender'){
                                    $arrayJugadores[1][] = $rowJugadores;
                                }else if($rowJugadores['position'] == 'midfielder'){
                                    $arrayJugadores[2][] = $rowJugadores;
                                }else if($rowJugadores['position'] == 'striker'){
                                    $arrayJugadores[3][] = $rowJugadores;     
                                }       
                           
                        }
                        ksort($arrayJugadores);
                    ?>
                    <h1 class="page-header"><?=$team['nombreEquipo']?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <?
                    $sql = "SELECT * FROM players WHERE idTeam=$idTeam";
                    $resultado = $mysqli->query($sql);          
                    $row_cnt = $resultado->num_rows;
                ?>
                <div class="col-lg-4 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-users fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?=$row_cnt?></div>
                                    <div>Jugadores</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-line-chart fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?=number_format($team['points'],0,".",".")?></div>
                                    <div>Puntos</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-eur fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?=number_format($team['value'],0,".",".")?> €</div>
                                    <div>Valor</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.panel -->
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped" id="tableJugadores">
                                            <thead>
                                                <tr>
                                                    <th>Posición</th>
                                                    <th>Jugadores</th>
                                                    <th>Puntos</th>
                                                    <th>Media</th>
                                                    <th>Valor</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?
                                            	foreach($arrayJugadores as $key => $value){
                                                    foreach($value as $player){
                                                        $media = "0";
                                                        if($player['partidos_jugados'] > 0)
                                                            $media = $player['points'] / $player['partidos_jugados'];
                                                    ?>
                                                    <tr>
                                                        <td><?=getPositionColor($player['position'])?></td>
                                                        <td> <?=$player['name']?></td>
                                                        <td><?=$player['points']?></td>
                                                        <td><?=number_format($media,1,",","")?></td>
                                                        <td><?=number_format($player['value'],0,".",".")?> €</td>
                                                    </tr>
                                                    <?
                                                    }
                                                }?>                         
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.col-lg-4 (nested) -->
                                <div class="col-lg-8">
                                    <div id="morris-bar-chart"></div>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

           	</div>     
        </div>
        <!-- /#page-wrapper -->

    </div>

</body>
  <?
include 'footer.php';

?>  

<script>
    $.tablesorter.addParser({
        // set a unique id
        id: 'puntos',
        is: function(s) {
            // return false so this parser is not auto detected
            return false;
        },
        format: function(s) {
            // format your data for normalization
            s=s.replace('.','');
            s=s.replace(',','.');
            return s;
        },
        // set type, either numeric or text
        type: 'numeric'
    });

    $.tablesorter.addParser({
        // set a unique id
        id: 'valor',
        is: function(s) {
            // return false so this parser is not auto detected
            return false;
        },
        format: function(s) {
            // format your data for normalization
            s=s.replace('€','');
            s=s.replace(new RegExp(/[.]/g), "");

            return s;
        },
        // set type, either numeric or text
        type: 'numeric'
    });

    $.tablesorter.addParser({
        // set a unique id
        id: 'media',
        is: function(s) {
            // return false so this parser is not auto detected
            return false;
        },
        format: function(s) {
            // format your data for normalization
            s=s.replace('.','');
            s=s.replace(',','.');
            return s;
        },
        // set type, either numeric or text
        type: 'numeric'
    });


    $(function() {
        $("#tableJugadores").tablesorter({
            headers: {
                2: {//zero-based column index
                    sorter:'puntos'
                },
                3: {
                    sorter:'media'
                },
                4: {
                    sorter:'valor'
                }
            }
        });
    });
</script>
