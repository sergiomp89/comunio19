<?
include 'header.php';
?> 
<body>
 <div id="wrapper">
  <?
  include 'menu.php';
  ?>
     <div id="page-wrapper">
         <div class="row">
             <div class="col-lg-12">
                 <h1 class="page-header">Mejorar equipo</h1>
             </div>
             <!-- /.col-lg-12 -->
         </div>
         <!-- /.row -->

         <div class="row">
             <!-- /.panel-heading -->

             <div class="panel-body">

                 <div class="row">

                         <legend>Elegir jugador humano</legend>
                         <div class="col-sm-3">
                             <!--desplegable para el numero de jugadores(hacerlo obligatorio)-->
                             Jugadores humanos:<br />
                             <select name="jugadoresHumanos" id="jugadoresHumanos" class="form-control" >
                                 <option value="0" selected="selected">Elige jugador</option>
                                 <?
                                 $sql = "SELECT id, name FROM human_players ORDER BY name asc";
                                 $resultado = $mysqli->query($sql);
                                 while($row = $resultado->fetch_assoc()) {
                                     ?>
                                     <option value="<?=$row['id']?>"><?=$row['name']?></option>
                                     <?
                                 }
                                 ?>
                             </select>
                         </div>

                         <div class="col-sm-2">
                             <!--desplegable para el numero de jugadores(hacerlo obligatorio)-->
                             Dinero:<br />
                            <input type="text" name="saldo" id="saldo" class="saldo form-control">
                         </div>
                         <div class="col-sm-3">
                             Criterio a maximizar:<br />
                             <select name="criterio" id="criterio" class="form-control">
                                 <option value="media" selected="selected">Media</option>
                                 <option value="racha">Racha</option>
                                 <option value="puntos">Numero de puntos</option>
                             </select>
                         </div>

                         <div class="col-sm-2">
                             Selección:<br />
                             <input type="radio" name="overheadRadio" value="presupuesto" checked> Valor mercado<br>

                             <input type="radio" name="overheadRadio" value="porcentaje"> Porcentaje<br>

                             <input type="radio" name="overheadRadio" value="cantidad"> Cantidad<br>

                             <input type="text" name="overheadPorcentajeOption" id="overheadPorcentajeOption" class="form-control" style="display: none;" onchange="handleChange(this);">

                             <input type="text" name="overheadCashOption" id="overheadCashOption" class="overheadCash form-control" style="display: none;">


                         </div>
                         <div class="col-sm-2">
                             <input type="button" id="submit" value="Mejorar"  style="margin-top: 25px;" class="btn btn-primary">

                         </div>
                 </div>




             </div>




         </div>


         <div class="row">
             <!-- /.panel -->
             <div class="panel panel-default">
                 <!-- /.panel-heading -->
                 <div class="panel-body">
                     <div class="row">
                         <div class="col-lg-6">
                             <div class="table-responsive">
                                 <table class="table table-bordered table-hover table-striped" id="tableEquiposHumano">
                                     <caption>Equipo Humano</caption>
                                     <thead>
                                     <tr>
                                         <th>Nombre</th>
                                         <th>Posición</th>
                                         <th>Puntos</th>
                                         <th>Valor</th>
                                         <th>Media</th>
                                         <th>Racha</th>
                                     </tr>
                                     </thead>
                                     <tbody id="humanTeam">
                                     </tbody>
                                 </table>
                             </div>
                             <!-- /.table-responsive -->
                         </div>
                         <div class="col-lg-6">
                             <div class="table-responsive">
                                 <table class="table table-bordered table-hover table-striped" id="tableEquipos">
                                     <caption>Mercado</caption>
                                     <thead>
                                     <tr>
                                         <th>Nombre</th>
                                         <th>Posición</th>
                                         <th>Puntos</th>
                                         <th>Valor</th>
                                         <th>Media</th>
                                         <th>Racha</th>
                                     </tr>
                                     </thead>
                                     <tbody id="market">
                                     <?php
                                     $sql = "SELECT j.id, j.name as nameJugador,j.position, j.status, j.value as valueJugador, j.points as pointsJugador, e.name as nameEquipo, j.partidos_jugados, j.racha FROM players j, teams e, market m where j.idTeam=e.id and j.id=m.id_player";
                                     $resultado = $mysqli->query($sql);
                                     while($row = $resultado->fetch_assoc()) {
                                         $idPlayer = $row['id'];
                                         $nameJugador = $row['nameJugador'];
                                         $position = getPosition($row['position']);
                                         $pointsJugador = number_format($row['pointsJugador'],0,".",".");
                                         $valueJugador = number_format($row['valueJugador'],0,".",".");

                                         $media = 0;
                                         if( $row['partidos_jugados'] > 0)
                                             $media = number_format($row['pointsJugador'] / $row['partidos_jugados'] ,2,".",".");

                                         $racha = $row['racha'];?>
                                         <tr>
                                         <td><?=$nameJugador?></td>
                                        <td><?=$position?></td>
                                         <td><?=$pointsJugador?></td>
                                         <td><?=$valueJugador?>€</td>
                                         <td><?=$media?></td>
                                         <td><?=$racha?></td>
                                          </tr>
                                     <?} ?>
                                     </tbody>
                                 </table>
                             </div>
                             <!-- /.table-responsive -->
                         </div>
                         <!-- /.col-lg-8 (nested) -->
                     </div>
                     <div class="row">
                        <div id="showCompareResume">

                        </div>
                     </div>
                     <!-- /.row -->
                 </div>

                 <!-- /.panel-body -->
             </div>
         </div>

     </div>

   </div>

</body>
  <?
include 'footer.php';

?>


<script>
    $(document).ready(function(){
        $('.saldo').mask('000.000.000.000.000', {reverse: true});
    });

    $(document).ready(function(){
        $('.overheadCash').mask('000.000.000.000.000', {reverse: true});
    });
</script>

<script>
    function handleChange(input) {
        if (input.value < 0) input.value = 0;
        if (input.value > 100) input.value = 100;
    }
</script>

<script type="text/javascript">

    function showFormacion(id) {
        if (document.getElementById) {
            var el = document.getElementById(id);
            el.style.display = (el.style.display == 'none') ? 'block' : 'none';
            window.scrollTo(0,document.body.scrollHeight);

        }
    }

    $(document).ready(function () {
        $("input:radio[name='overheadRadio']").change(function(){
            if ($(this).val() == 'porcentaje') {
                $('#overheadPorcentajeOption').show();
                $('#overheadCashOption').hide();
                $('#overheadCashOption').val('');

            }
            if ($(this).val() == 'cantidad') {
                $('#overheadPorcentajeOption').hide();
                $('#overheadCashOption').show();
                $('#overheadPorcentajeOption').val('');

            }
            if ($(this).val() == 'presupuesto') {
                $('#overheadPorcentajeOption').hide();
                $('#overheadCashOption').hide();
                $('#overheadPorcentajeOption').val('');
                $('#overheadCashOption').val('');


            }
        });


    });


    $( "#jugadoresHumanos" ).change(function() {
        var idHumanPlayer = $("#jugadoresHumanos").val();
        var parametros = {
            "idHumanPlayer" : idHumanPlayer
        };
        $.ajax({
            url: "showHumanPlayerTeam.php",
            data: parametros,
            dataType:"html",
            type: "post",
            success: function(data){
                $('#humanTeam').html(data);
            }
        });
    });





    $( "#submit" ).click(function() {

        var idHumanPlayer = $("#jugadoresHumanos").val();
        var saldo = $("#saldo").val();
        var criterio = $("#criterio").val();
        var overheadPorcentajeOption = $("#overheadPorcentajeOption").val();
        var overheadCashOption = $("#overheadCashOption").val();
        var overheadRadio = $('input[name=overheadRadio]:checked').val();

        var parametros = {
            "idHumanPlayer" : idHumanPlayer,
            "saldo": saldo,
            "criterio": criterio,
            "overheadPorcentajeOption": overheadPorcentajeOption,
            "overheadCashOption": overheadCashOption,
            "overheadRadio":overheadRadio
        };
      /*  $.ajax({
            url: "showHumanPlayerTeam.php",
            data: parametros,
            dataType:"html",
            type: "post",
            success: function(data){
                $('#humanTeam').html(data);
            }
        });
*/
        /*$.ajax({
            url: "showMarket.php",
            data: parametros,
            dataType:"html",
            type: "post",
            success: function(data){
                $('#market').html(data);
            }
        });*/


        $.ajax({
            url: "compareTeam.php",
            data: parametros,
            dataType:"html",
            type: "post",
            success: function(data){
                $('#showCompareResume').html(data);
            }
        });

        window.scrollTo(0,document.body.scrollHeight);
    });


</script>

