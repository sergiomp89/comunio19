
    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <script src="vendor/bootstrap/js/bootstrap-confirmation.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>
    <script src="dist/js/jquery-dataTable.js"></script>
    <script type="text/javascript" src="dist/js/jquery.mask.min.js"></script>
    <script src="dist/js/jquery.tablesorter.min.js"></script>
    <script src="dist/js/tablesorter-widget.js"></script>
    <script src="dist/js/scrollTo.js"></script>

    <script src="dist/js/jquery.validate.js"></script>
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>


</body>

</html>
