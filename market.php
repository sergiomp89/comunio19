<?
include 'header.php';
?>
<body>
<div id="wrapper">
    <?
    include 'menu.php';
    ?>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Mercado</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row">
                    <form id="addPlayerToMarket">
                        <legend>Añadir jugador</legend>
                        <div class="col-sm-3">
                            <label for="jugador">Jugador:</label>
                            <select name="jugador" id="jugador" class="form-control" >
                                <option value="0" selected="selected">Elige jugador</option>
                                <?
                                $sql = "SELECT j.id, j.name as nameJugador,j.position, e.name as nameEquipo FROM players j, teams e where j.idTeam=e.id and j.id not in (SELECT id_player from human_players_team) and j.id not in (SELECT id_player from market) order by j.idTeam asc, j.name asc";
                                $resultado = $mysqli->query($sql);
                                while($row = $resultado->fetch_assoc()) {
                                    ?>
                                    <option value="<?=$row['id']?>"><?=$row['nameJugador']?> - <?=$row['nameEquipo']?></option>
                                    <?
                                }
                                ?>
                            </select>

                        </div>
                        <div class="col-sm-2">
                            <label for=""></label>
                             <input id="filterPlayers" style="width:255px;margin-top: 5px;" class="form-control">
                        </div>

                        <div class="col-sm-3">
                            <input type="button" style="margin-top: 25px;" id="submit" value="Añadir" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <!-- /.panel -->
            <div class="panel panel-default">
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped" id="tableEquipos">
                                    <thead>
                                    <tr>

                                        <th>Nombre</th>
                                        <th>Equipo</th>
                                        <th>Posición</th>
                                        <th>Estado</th>
                                        <th>Puntos</th>
                                        <th>Valor</th>
                                        <th>Acción</th>
                                    </tr>
                                    </thead>
                                    <tbody id="market">
                                    <?
                                    $sql = "SELECT j.id, j.name as nameJugador,j.position, j.status, j.value as valueJugador, j.points as pointsJugador, e.name as nameEquipo FROM players j, teams e, market m where j.idTeam=e.id and j.id=m.id_player order by j.idTeam asc, j.name asc";
                                    $resultado = $mysqli->query($sql);
                                    while($row = $resultado->fetch_assoc()) {
                                        ?>
                                        <tr id="player<?=$row['id']?>">

                                            <td><?=$row['nameJugador']?></td>
                                            <td><?=$row['nameEquipo']?></td>
                                            <td><?=getPosition($row['position'])?></td>
                                            <td><?=getStatus($row['status'])?></td>
                                            <td><?=number_format($row['pointsJugador'],0,".",".")?></td>
                                            <td><?=number_format($row['valueJugador'],0,".",".")?> €</td>
                                            <td><a class="btn btn-danger" role="button" onclick="deletePlayerToMarket(<?=$row['id']?>)">Eliminar</a></td>
                                        </tr>
                                        <?
                                    }
                                    ?>


                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.col-lg-4 (nested) -->
                        <div class="col-lg-8">
                            <div id="morris-bar-chart"></div>
                        </div>
                        <!-- /.col-lg-8 (nested) -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.panel-body -->
            </div>
        </div>

    </div>

</div>

</body>
<?
include 'footer.php';

?>


<script type="text/javascript">

    $( "#submit" ).click(function() {
        var idPlayer = $("#jugador").val();
        if(idPlayer != 0){
            var parametros = {
                "idPlayer" : idPlayer
            };
            $.ajax({
                url: "addPlayerToMarket.php",
                data: parametros,
                dataType:"html",
                type: "post",
                success: function(data){
                    $('#market').html(data);
                    $.ajax({
                        url: "refreshSelectPlayersMarket.php",
                        dataType:"html",
                        success: function(data){
                            $('#jugador').html(data);

                        }
                    });
                }
            });
        }

    });

    function deletePlayerToMarket(idPlayer){
        var parametros = {
            "idPlayer" : idPlayer
        };
        $.ajax({
            url: "deletePlayerToMarket.php",
            data: parametros,
            dataType:"html",
            type: "post",
            success: function(data){
                $('#player'+idPlayer).remove();
                $.ajax({
                    url: "refreshSelectPlayersMarket.php",
                    dataType:"html",
                    success: function(data){
                        $('#jugador').html(data);

                    }
                });
            }
        });
    }



</script>

<script type="text/javascript">

    $(function () {
        var opts = $('#jugador option').map(function () {
            return [[this.value, $(this).text()]];
        });
        $('#filterPlayers').keyup(function () {
            var rxp = new RegExp($('#filterPlayers').val(), 'i');
            var optlist = $('#jugador').empty();
            opts.each(function () {
                if (rxp.test(this[1])) {
                    optlist.append($('<option/>').attr('value', this[0]).text(this[1]));
                }
            });

        });

    });

    $.tablesorter.addParser({
        // set a unique id
        id: 'puntos',
        is: function(s) {
            // return false so this parser is not auto detected
            return false;
        },
        format: function(s) {
            // format your data for normalization
            s=s.replace('.','');
            s=s.replace(',','.');
            return s;
        },
        // set type, either numeric or text
        type: 'numeric'
    });

    $.tablesorter.addParser({
        // set a unique id
        id: 'valor',
        is: function(s) {
            // return false so this parser is not auto detected
            return false;
        },
        format: function(s) {
            // format your data for normalization
            s=s.replace('€','');
            s=s.replace(new RegExp(/[.]/g), "");
            return s;
        },
        // set type, either numeric or text
        type: 'numeric'
    });

    $(function() {
        $("#tableEquipos").tablesorter({
            headers: {
                4: {//zero-based column index
                    sorter:'puntos'
                },
                5: {
                    sorter:'valor'
                }
            }
        });
    });
</script>
