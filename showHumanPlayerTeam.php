<?php
include 'conexionDB.php';
include 'lib.php';

$idHumanPlayer = $_POST['idHumanPlayer'];


//Get ultima jornada
$ultimaJornada = 0;
$sql = "SELECT DISTINCT jornada FROM players_historico order by jornada desc LIMIT 1";
$resultado = $mysqli->query($sql);
while($row = $resultado->fetch_assoc()) {
    $ultimaJornada = $row['jornada'];
}

$data ='';
$sql = "SELECT j.id, j.name as nameJugador,j.position, j.status, j.value as valueJugador, j.points as pointsJugador, e.name as nameEquipo, j.partidos_jugados FROM players j, teams e, human_players_team hpt where j.idTeam=e.id and j.id=hpt.id_player and hpt.id_human_player=$idHumanPlayer";
$resultado = $mysqli->query($sql);
while($row = $resultado->fetch_assoc()) {
    $idPlayer = $row['id'];
    $nameJugador = $row['nameJugador'];
    $position = getPosition($row['position']);
    $pointsJugador = number_format($row['pointsJugador'],0,".",".");
    $valueJugador = number_format($row['valueJugador'],0,".",".");

    $media = 0;
    if( $row['partidos_jugados'] > 0)
    $media = number_format($row['pointsJugador'] / $row['partidos_jugados'] ,2,".",".");


    $racha = 0;
    if($ultimaJornada > 0){
        $totalPoints = 0;
        for($i = $ultimaJornada; $i > $ultimaJornada-5; $i--){
            $sqlRacha = "SELECT points FROM players_historico where playerId=$idPlayer and jornada=$i";
            $resultadoRacha = $mysqli->query($sqlRacha);

            if($resultadoRacha){
                while($rowRacha = $resultadoRacha->fetch_assoc()) {
                    $totalPoints = $totalPoints + $rowRacha['points'];
                }
            }

        }

        $racha = $totalPoints / 5;
    }


    $data .=' <tr>';
    $data .='<td>'.$nameJugador.'</td>';
    $data .='<td>'.$position.'</td>';
    $data .='<td>'.$pointsJugador.'</td>';
    $data .='<td>'.$valueJugador.' €</td>';
    $data .='<td>'.$media.'</td>';
    $data .='<td>'.$racha.'</td>';

    $data .=' </tr>';
}
echo $data;