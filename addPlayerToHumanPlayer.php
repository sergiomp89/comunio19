<?php
include 'conexionDB.php';
include 'lib.php';

$idPlayer = $_POST['idPlayer'];
$idHumanPlayer = $_POST['idHumanPlayer'];

$query = "INSERT INTO human_players_team (id_player,id_human_player) VALUES ($idPlayer,$idHumanPlayer)";
$mysqli->query($query);



$data ='';
$sql = "SELECT j.id, j.name as nameJugador,j.position, j.status, j.value as valueJugador, j.points as pointsJugador, e.name as nameEquipo FROM players j, teams e, human_players_team hpt where j.idTeam=e.id and j.id=hpt.id_player and hpt.id_human_player=$idHumanPlayer";
$resultado = $mysqli->query($sql);
while($row = $resultado->fetch_assoc()) {
    $idPlayer = $row['id'];
    $nameJugador = $row['nameJugador'];
    $nameEquipo = $row['nameEquipo'];
    $position = getPosition($row['position']);
    $status = getStatus($row['status']);
    $pointsJugador = number_format($row['pointsJugador'],0,".",".");
    $valueJugador = number_format($row['valueJugador'],0,".",".");
    $id ='player'.$idPlayer;
    $data .="<tr id=".$id.">";
    $data .='<td>'.$nameJugador.'</td>';
    $data .='<td>'.$nameEquipo.'</td>';
    $data .='<td>'.$position.'</td>';
    $data .='<td>'.$status.'</td>';
    $data .='<td>'.$pointsJugador.'</td>';
    $data .='<td>'.$valueJugador.' €</td>';
    $data .='<td><a class="btn btn-danger" role="button" onclick="deletePlayerToHumanPlayer('.$idPlayer.','.$idHumanPlayer.')">Eliminar</a></td>';
    $data .=' </tr>';
}
echo $data;