<?
include 'header.php';
?> 
<body>
 <div id="wrapper">
  <?
  include 'menu.php';
  ?> 
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Jugadores</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <!-- /.panel -->
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <input type="text" id="search" style="width:200px"  class="form-control"/>
                                        <br>
                                        <table class="table table-bordered table-hover table-striped tablesorter" id="tableJugadores">
                                            <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Equipo</th>
                                                    <th>Posición</th>
                                                    <th>Estado</th>
                                                    <th>Puntos</th>
                                                    <th>Valor</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            	<?
                                            		$sql = "SELECT j.id, j.name as nameJugador,j.position, j.status, j.value as valueJugador, j.points as pointsJugador, e.name as nameEquipo FROM players j, teams e where j.idTeam=e.id";
                                            		$resultado = $mysqli->query($sql);
                                            		while($row = $resultado->fetch_assoc()) {
                                            			?>
                                            			<tr>
		                                                    <td><?=$row['nameJugador']?></td>
                                                            <td><?=$row['nameEquipo']?></td>
                                                            <td><?=getPosition($row['position'])?></td>
                                                            <td><?=getStatus($row['status'])?></td>
		                                                    <td><?=number_format($row['pointsJugador'],0,".",".")?></td>
		                                                    <td><?=number_format($row['valueJugador'],0,".",".")?> €</td>
		                                                </tr>
		                                                <?
                                            		}
                                            	?>
                                                
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.col-lg-4 (nested) -->
                                <div class="col-lg-8">
                                    <div id="morris-bar-chart"></div>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

           	</div>     
        </div>
        <!-- /#page-wrapper -->

    </div>

</body>
  <?
include 'footer.php';

?>
<script>
    $.tablesorter.addParser({
        // set a unique id
        id: 'puntos',
        is: function(s) {
            // return false so this parser is not auto detected
            return false;
        },
        format: function(s) {
            // format your data for normalization
            s=s.replace('.','');
            s=s.replace(',','.');
            return s;
        },
        // set type, either numeric or text
        type: 'numeric'
    });

    $.tablesorter.addParser({
        // set a unique id
        id: 'valor',
        is: function(s) {
            // return false so this parser is not auto detected
            return false;
        },
        format: function(s) {
            // format your data for normalization
            s=s.replace('€','');
            s=s.replace(new RegExp(/[.]/g), "");
            return s;
        },
        // set type, either numeric or text
        type: 'numeric'
    });

    $("#search").keyup(function(){
        _this = this;
        // Muestra los tr que concuerdan con la busqueda, y oculta los demás.
        $.each($("#tableJugadores tbody tr"), function() {
            if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
                $(this).hide();
            else
                $(this).show();
        });
    });

    $(function() {
         $("#tableJugadores").tablesorter({
            headers: {
                4: {//zero-based column index
                    sorter:'puntos'
                },
                5: {
                    sorter:'valor'
                }
            }
        });
    });
</script>

